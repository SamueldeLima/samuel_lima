import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators as vt } from '@angular/forms';
import { IonicSafeString, NavController, ToastController } from '@ionic/angular';
import { ContaService } from '../conta.service';


@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

  contasForm: FormGroup;

  validators = {
    parceiro: ['', [vt.required, vt.minLength(5)]],
    descricao: ['', [vt.required, vt.minLength(6)]],
    valor: ['', [vt.required, vt.min(0.01)]],
    tipo: ['', vt.required]
    
  }

  constructor(
    private builder: FormBuilder,
    private contaService: ContaService,
    private nav: NavController,
    private toast: ToastController
  ) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.contasForm = this.builder.group(this.validators);
  }

  /**
   * Salva a nova conta no Firebase.
   * 
   */
  async registraConta() {
    const conta = this.contasForm.value;
    try {
      await this.contaService.registraConta(conta);
      this.nav.navigateForward('contas/pagar');
    } catch (e: unknown) {
      if (e instanceof Error) this.showToast(e.message);
    }
  }

  private async showToast(message: string) {
    const ctrl = await this.toast.create({
      message: message,
      duration: 3000
  });

  ctrl.present();
  }


}
