import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { ContaService } from '../conta.service';

@Component({
  selector: 'app-pagar',
  templateUrl: './pagar.page.html',
  styleUrls: ['./pagar.page.scss'],
})
export class PagarPage implements OnInit {
  listaContas;

  constructor(
    private contaService: ContaService
  ) { }

  ngOnInit(): void {
    this.contaService.lista("pagar").subscribe(lista => {this.listaContas = lista});
  }

  remove(conta) {
    this.contaService.remove(conta);
  }
}
