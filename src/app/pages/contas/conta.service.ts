import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class ContaService {

  collection: AngularFirestoreCollection;

  constructor(
    private db: AngularFirestore
  ) { }

  registraConta(conta: any) {
    const id = this.db.createId();
    conta.id = id;
    this.collection = this.db.collection('conta');
    let promise = this.collection.doc(id).set(conta);
    return promise;
    
  }

  lista(tipo: String) {
    this.collection = this.db.collection('conta', ref => ref.where('tipo', '==', tipo));
    return this.collection.valueChanges();
  }

  remove(conta: any) {
    this.collection = this.db.collection('conta');
    this.collection.doc(conta.id).delete();
  }
}
