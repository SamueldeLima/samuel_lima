// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCTmL05Aqw3_d-TlCKEKVKOqLgJp95sUOg",
    authDomain: "controle-if-gu3012565.firebaseapp.com",
    projectId: "controle-if-gu3012565",
    storageBucket: "controle-if-gu3012565.appspot.com",
    messagingSenderId: "1034908162496",
    appId: "1:1034908162496:web:699b5437ac270ca6301025",
    measurementId: "G-371TVK1B9L"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
